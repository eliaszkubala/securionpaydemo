import { validateCardExpirationDate } from '../src/components/utils';


beforeEach(() => {
    const mockedDate = new Date('2020-07-14T11:01:58.135Z')
    // @ts-ignore
    jest.spyOn(global, 'Date').mockImplementation(() => mockedDate)
})

it('Test months', () => {
    expect(validateCardExpirationDate("2")).toEqual(false)
    expect(validateCardExpirationDate("13")).toEqual(false)

    expect(validateCardExpirationDate("02")).toEqual(true)

    expect(validateCardExpirationDate("1")).toEqual(true) // followed by one can be 11, 12
    expect(validateCardExpirationDate("12")).toEqual(true)
});

it('Month from past with current year is wrong', () => {
    expect(validateCardExpirationDate("0420")).toEqual(false)
    expect(validateCardExpirationDate("0520")).toEqual(false)
})

it('Past year is wrong', () => {
    expect(validateCardExpirationDate("1219")).toEqual(false)
    expect(validateCardExpirationDate("1217")).toEqual(false)
})

it('Correct month and year', () => {
    expect(validateCardExpirationDate("0720")).toEqual(true) // i am not sure if this case should be accepted
    expect(validateCardExpirationDate("1220")).toEqual(true)

})
