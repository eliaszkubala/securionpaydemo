import React = require("react");
import { Container } from "react-bootstrap";
import { hot } from "react-hot-loader";
import "./../assets/scss/App.scss";
import { configureIcons } from "./AwesomeIcons";
import { CardNumberContainer, OnChange } from "./CardNumber";
configureIcons()

const App = () => {

  const onChange: OnChange = (cardNumber, expirationDate, cvc) =>
    console.log('onChange', `${cardNumber} / ${expirationDate} / ${cvc}`)

  const onSuccess: OnChange = (cardNumber, expirationDate, cvc) => {
    const msg = `${cardNumber} / ${expirationDate} / ${cvc}`;
    alert(`Sucessfully filled up with ${msg}`)
    console.log('onSuccess')
  }

  return <Container className="app" style={{ textAlign: 'center', width: 700 }}>
    <CardNumberContainer
      onChange={onChange}
      onSuccess={onSuccess}
    />

    <p className='mt-2' style={{ color: 'grey' }}>feel free to open console and check logs</p>
  </Container>
}




declare let module: object;

export default hot(module)(App);
