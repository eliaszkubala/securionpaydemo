
import React = require('react')
import { IconProp } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import * as _ from 'lodash'
import { useCallback, useEffect, useState } from 'react'
import { Card, Form, FormControl, FormControlProps, InputGroup } from 'react-bootstrap'
import { validateCardExpirationDate } from './utils'

export type OnChange = (cardNumber: string, expirationDate: string, cvc: string) => void

interface OwnProps {
    onChange: OnChange,
    onSuccess?: OnChange,
}

export const CardNumberContainer = ({
    onChange, onSuccess,
}: OwnProps) => {

    const [icon, setIcon] = useState<IconProp>(undefined)

    const cvcRef = React.useRef(undefined)
    const expirationDateRef = React.useRef(undefined)
    const cardNumberRef = React.useRef(undefined)
    const getFirstCardNumber = () => _.toNumber(_.first(cardNumberState) || 0)

    const [cvcState, setCvcState] = useState<string>('')
    const [cardNumberState, setNumberValueState] = useState<string>('')
    const [expirationDateState, setExpirationDateState] = useState<string>('')

    useEffect(() => {
        if (cvcState.length === 3 &&
            cardNumberState.length === 4 * 4 &&
            expirationDateState.length === 4) {
            onSuccess(cardNumberState, expirationDateState, cvcState)
        }
        onChange(cardNumberState, expirationDateState, cvcState)
    }, [cvcState, cardNumberState, expirationDateState])

    useEffect(() => {
        setIcon(
            getFirstCardNumber() === 4 ? ['fab', 'cc-visa']
                : getFirstCardNumber() === 5 ? ['fab', 'cc-mastercard']
                    : 'credit-card'
        )
    }, [cardNumberState])

    const cardNumberFormatted = useCallback(() => {
        return _.chunk(cardNumberState, 4).map(item => item.join("")).join(" ")
    }, [cardNumberState])

    const cardNumberChangeCallback = useCallback((event: React.FormEvent<FormControlProps>) => {
        const value = event.currentTarget.value as string
        const newValue = value.replace(/\D/g, "")

        if (newValue.length <= 16) {
            setNumberValueState(newValue)
        }

        if (newValue.length >= 16) {
            expirationDateRef.current.focus()
        }

    }, [setNumberValueState])

    // cvc number


    const expirationDateFormattedCallback = useCallback(() => {
        return _.chunk(expirationDateState, 2).map(item => item.join("")).join("/")
    }, [expirationDateState])

    const expirationDateChangeCallback = useCallback((event: React.FormEvent<FormControlProps>) => {
        const value = event.currentTarget.value as string
        const newValue = value.replace(/\D/g, "")

        if (validateCardExpirationDate(newValue)) {
            setExpirationDateState(newValue)
        }

        if (newValue.length === 4) {
            cvcRef.current.focus()
        }

    }, [expirationDateState, setExpirationDateState])

    // cvc

    const cvcFormattedCallback = useCallback(() => cvcState, [cvcState])

    const cvcChangeCallback = useCallback((event: React.FormEvent<FormControlProps>) => {
        const value = event.currentTarget.value as string
        const newValue = value.replace(/\D/g, "")
        if (newValue.length <= 3) {
            setCvcState(newValue)
        }

    }, [setCvcState])

    const onBackspacePressCallback = useCallback(
        (moveFocusTo: React.MutableRefObject<any>, value: string) =>
            ({ keyCode }: { keyCode: number }) => {
                keyCode === 8 && !value.length && moveFocusTo.current.focus()
            }, [])

    return <Card md={6}>

        <Card.Body>

            <Form inline >

                <InputGroup>

                    <InputGroup.Prepend>
                        <InputGroup.Text style={{ width: 50, textAlign: 'center' }}>
                            <FontAwesomeIcon icon={icon} />
                        </InputGroup.Text>
                    </InputGroup.Prepend>

                    <FormControl
                        ref={cardNumberRef}
                        placeholder="Card Number"
                        onChange={cardNumberChangeCallback}
                        value={cardNumberFormatted()}
                    />

                    <FormControl
                        ref={expirationDateRef}
                        placeholder="MM/YY"
                        onChange={expirationDateChangeCallback}
                        value={expirationDateFormattedCallback()}
                        onKeyDown={onBackspacePressCallback(cardNumberRef, expirationDateState)}
                    />

                    <FormControl
                        ref={cvcRef}
                        placeholder="CVC"
                        onChange={cvcChangeCallback}
                        value={cvcFormattedCallback()}
                        onKeyDown={onBackspacePressCallback(expirationDateRef, cvcState)}
                    />

                </InputGroup>
            </Form>
        </Card.Body>

    </Card>


}

