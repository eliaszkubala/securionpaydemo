import { library } from '@fortawesome/fontawesome-svg-core'
import { fab, faBuffer, faCcMastercard, faCcVisa } from '@fortawesome/free-brands-svg-icons'
import { faCreditCard } from '@fortawesome/free-solid-svg-icons'

export const configureIcons = () => library.add(fab, faCcMastercard, faCcVisa, faBuffer, faCreditCard)