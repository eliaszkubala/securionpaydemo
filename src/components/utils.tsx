import _ = require('lodash')

export const validateCardExpirationDate = (value: string) => {
    const filledWithZero = _.range(4).map((i => _.toNumber(value[i]) || 0))
    const touched = _.range(4).map((i => value[i] !== undefined))

    var mmyy = _.chunk(filledWithZero, 2)

    var mm = mmyy[0]
    var yy = mmyy[1]
    const mmN = _.toNumber(mm.join(''))
    const yyN = _.toNumber(yy.join(''))

    const currentYear = new Date().getFullYear() - 2000
    const currentMonth = new Date().getMonth() + 1
    const currentYearFirstDigit = _.toInteger(currentYear / 10)

    if (value.length <= 4) {

        if (mm[0] > 1)
            return false

        if (touched[1] && (mmN > 12 || mmN === 0))
            return false

        if (touched[2] && !touched[3] && yy[0] < currentYearFirstDigit)
            return false


        if (touched[3] && (yyN < currentYear))
            return false

        if (touched[3] && mmN < currentMonth && yyN === currentYear)
            return false

    }

    return true
}